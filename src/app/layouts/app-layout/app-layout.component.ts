import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs';
import { AuthenticationStore } from 'src/modules/authentication/authentication.store';
import { WebsocketConnection } from 'src/modules/common/WebsocketConnection';
import { NotificationStore } from 'src/modules/notification/notification.store';
import { HttpNotificationQueries } from 'src/modules/notification/services/platform/http/notification.queries.http';
import { AnyNotification } from "src/modules/notification/notification.model";
import { NotificationSocketService } from 'src/modules/notification/services/notification.socket.service';
import { NzMessageService } from "ng-zorro-antd/message";
import { NotificationWebService } from 'src/app/notification-web.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-app-layout',
  templateUrl: './app-layout.component.html',
  styleUrls: ['./app-layout.component.less']
})
export class AppLayoutComponent implements OnInit, OnDestroy {
  sub?: Subscription;
  notifications : AnyNotification[];
  notificationOpen : boolean = false;
  showDrawer: boolean = false;

  constructor(private router: Router,
    private notificationWebService : NotificationWebService, 
    private nzMessageService: NzMessageService, 
    private socket: WebsocketConnection, 
    private authStore: AuthenticationStore, 
    private notificationQueries : HttpNotificationQueries,
    private notificationSocket : NotificationSocketService,
    private notificationStore : NotificationStore) {

    this.notificationWebService.getPermission()
    this.notificationSocket.onNewNotification((notif : AnyNotification) => {
      this.displayNotification(notif);
    });

  }

  displayNotification(notif : AnyNotification)
  {

    let messageInfo : string = "";
    switch(notif.subject)
    {
      case "new_user" : messageInfo = "Il y a un nouveau venu !"; break;
      case "post_liked" : messageInfo = "Un post à été liké"; break;
      case "room_added" : messageInfo = "Une salle à été crée"; break;
    }

    if(document.visibilityState === 'hidden')
    {
      if(this.notificationWebService.havePermission())
      {
          let n : Notification = this.notificationWebService.createNotification(messageInfo);
          this.notificationWebService.createEventNotification(n, () => {
            switch(notif.subject)
            {
              case "post_liked" : messageInfo = "Un post à été liké"; break;
              case "room_added" : this.router.navigateByUrl("/app/" + notif.payload.room.id); break;
            }

            window.focus()
          });
       }
    }
    else{
      this.nzMessageService.info(messageInfo);
    }
  }
  ngOnInit(): void {
    this.sub = this.authStore.accessToken$.subscribe(accessToken => {
      if (accessToken) {
        this.socket.connect(accessToken);
      } else {
        this.socket.disconnect();
      }
    });
  }

  ngOnDestroy() {
    if (this.sub) {
      this.sub.unsubscribe();
    }
  }
  
  async onToggleNotifications() {
    this.showDrawer = !this.showDrawer;
    this.notifications = await this.notificationQueries.getNotifications();
    this.notificationOpen = !this.notificationOpen;
  };
  
}
