import { TestBed } from '@angular/core/testing';

import { NotificationWebService } from './notification-web.service';

describe('NotificationWebService', () => {
  let service: NotificationWebService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(NotificationWebService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
