import { Injectable } from '@angular/core';
import { NotificationCommands } from 'src/modules/notification/services/notification.commands';

@Injectable({
  providedIn: 'root'
})
export class NotificationWebService {

  public authorized : any;
  constructor() { 
 
  }

  createEventNotification(n : Notification, fn : Function) : void 
  {
    n.addEventListener("click", () => { fn() });
  }

  createNotification(message : string) : Notification 
  {
    return new Notification(message);
  }

  havePermission() : boolean
  {
    if(Notification.permission === 'denied' || Notification.permission === 'default')
    {
      return false;
    }
    return true;
  }

  async getPermission() : Promise<NotificationPermission>
  {
    this.authorized = await Notification.requestPermission();
    return this.authorized;
  }
}
