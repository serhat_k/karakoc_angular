import { MessageAudioElement, MessageElement, MessageImageElement, MessageLinkElement, MessageTextElement, MessageVideoElement, MessageYoutubeElement, Post, PostData, PostMessage } from '../post.model';

export class PostMapper {
  map(data: PostData): Post {
    return {
      ...data,
      message: this.parseMessage(`${data.message} ${data.attachementUrl ? data.attachementUrl : ''}`)
    }
  }

  private parseMessage(message: string): PostMessage {
    let itsJustLink : boolean = true;
    // TODO rajouter png jpg et gif
    const pictureRegex = /http[s]?:\/\/.+\.(jpeg|jpg|png|gif)/gmi;

     // TODO mp4,wmv,flv,avi,wav
    const videoRegex = /http[s]?:\/\/.+\.(mp4|wmv|flv|avi|wav)/gmi;

     // TODO mp3,ogg,wav
    const audioRegex = /http[s]?:\/\/.+\.(mp3|ogg|wav)/gmi;

    const youtubeRegex = /(http[s]?:\/\/)?www\.(?:youtube\.com\/\S*(?:(?:\/e(?:mbed))?\/|watch\/?\?(?:\S*?&?v\=))|youtu\.be\/)([a-zA-Z0-9_-]{6,11})/gmi;

    const url = /http[s]?:\/\/.+\.[a-zA-z]{2,10}\/?/gmi;

    const attachements: MessageElement[] = [];

    const pictureMatche = pictureRegex.exec(message);
    if (pictureMatche) {
      let attachement: MessageImageElement = {
        type : "image",
        url : pictureMatche[0]
      };
      attachements.push(attachement);
      itsJustLink = !itsJustLink;
     // TODO ajouter un attachement de type image dans attachements
    }

    const videoMatche = videoRegex.exec(message)
    if (videoMatche) {
     // TODO ajouter un attachement de type video dans attachements
     let attachement: MessageVideoElement = {
       type : "video",
       url : videoMatche[0]
     };
      attachements.push(attachement);
      itsJustLink = !itsJustLink;

    }

    const audioMatche = audioRegex.exec(message)
    if (audioMatche) {
     // TODO ajouter un attachement de type audio dans attachements
     let attachement: MessageAudioElement = {
       type : "audio",
      url : audioMatche[0]
     }
     attachements.push(attachement);
     itsJustLink = !itsJustLink;

    }

    const youtubeMatche = youtubeRegex.exec(message)
    if (youtubeMatche) {
     // TODO ajouter un attachement de type youtube dans attachements
      let attachement: MessageYoutubeElement={
        type: "youtube",
        videoId: youtubeMatche[2]
      }
      attachements.push(attachement);
      itsJustLink = !itsJustLink;

    }

    // if(itsJustLink)
    // {
    //   let result : RegExpExecArray | null;
    //   let messageTemp : string = message;
    //   let attachement: MessageLinkElement;
    //   result = url.exec(messageTemp);
    //   while(result)
    //   {
    //     attachement = {
    //       type:"link",
    //       url: result[0]
    //     }
    //     attachements.push(attachement);
    //     messageTemp = messageTemp.replace(result[0], "");
    //     result = url.exec(messageTemp);
    //   }
    // }

    return {
      text: {
        type: 'text',
        content: message
      } as MessageTextElement,
      attachements
    };
  }
}
