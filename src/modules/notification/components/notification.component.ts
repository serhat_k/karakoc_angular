import { Component, Input, OnInit } from '@angular/core';
import { UserQueries } from 'src/modules/user/services/user.queries';
import { UserService } from 'src/modules/user/services/user.service';
import { AnyNotification } from '../notification.model';
import {User} from 'src/modules/user/user.model';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.less']
})
export class NotificationComponent implements OnInit {

  @Input()
  notifications : AnyNotification[];
  
  constructor(private userQueries : UserQueries) {}

  ngOnInit(): void {
    this.getUser();
    console.log(this.notifications);
  }


  async getUser() : Promise<User[]>
  {
    let testUser : User[] = await this.userQueries.search(this.notifications[0].payload.user.username);

    console.log("-> result ");
    console.log(testUser);
    return testUser;
  }



}
