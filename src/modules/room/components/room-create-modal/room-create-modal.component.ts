import { Route } from '@angular/compiler/src/core';
import { Component, OnInit, EventEmitter, Output, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { Room, RoomType } from '../../room.model';
import { RoomService } from '../../services/room.service';
import { RoomMenuComponent } from '../room-menu/room-menu.component';

export class CreateRoomFormModel {
  name: string = "";
  type: RoomType = RoomType.Text;
}

@Component({
  selector: 'app-room-create-modal',
  templateUrl: './room-create-modal.component.html',
  styleUrls: ['./room-create-modal.component.less']
})
export class RoomCreateModalComponent implements OnInit {
  @ViewChild("f")
  form: NgForm;

  @Output()
  lastAddRoom : EventEmitter<Room> = new EventEmitter();

  isVisible: boolean = false;
  model = new CreateRoomFormModel();

  constructor(private roomService: RoomService, private router: Router) {

  }

  ngOnInit(): void {
  }

  async onOk() {
    if (this.form.form.valid) {
      let answerCreateRoom : any = await this.roomService.create(this.model.name, this.model.type);
      console.log(answerCreateRoom);
      if(answerCreateRoom)
      {
        localStorage.setItem("roomId", answerCreateRoom.id);
        this.router.navigateByUrl("/app/" + answerCreateRoom.id);
        this.lastAddRoom.emit(answerCreateRoom);
      }

      this.close();

    }
  }

  onCancel() {
    this.close();
  }

  open() {
    this.form.resetForm(new CreateRoomFormModel());
    this.isVisible = true;
  }

  close() {
    this.isVisible = false;
  }
}
