import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { FeedStore } from 'src/modules/feed/feed.store';
import { Room } from '../../room.model';
import { RoomStore } from '../../room.store';
import { RoomQueries } from '../../services/room.queries';
import { RoomService } from '../../services/room.service';
import { RoomSocketService } from '../../services/room.socket.service';
@Component({
  selector: 'app-room-menu',
  templateUrl: './room-menu.component.html',
  styleUrls: ['./room-menu.component.less']
})
export class RoomMenuComponent implements OnInit {
  roomId$: Observable<string | undefined>;

  rooms: Room[];

  constructor(private feedStore: FeedStore, private queries: RoomQueries, private roomSocketService: RoomSocketService ,private router : Router) {
    this.roomId$ = feedStore.roomId$;
    this.rooms = [];
    // this.roomSocketService.onNewRoom(() => {     this.rooms.push(this.lastAddRoom)    });
  }

  addRoom(lastAddRoom : Room)
  {
    this.rooms.push(lastAddRoom);
  }
  
  async ngOnInit() {
    let lastRoom : string | null = localStorage.getItem('roomId');
    this.rooms = await this.queries.getAll();

    if( lastRoom == null || !this.feedStore.value.roomId)
    {
      this.router.navigate(['app/'+this.rooms[0].id]);
    }
    else
    {
      this.router.navigate(['app/'+ lastRoom]);
    }
  }
  
  goToRoom(room: Room) {
    this.router.navigateByUrl("/app/" + room.id);
    localStorage.setItem('roomId', room.id);
  }
}
