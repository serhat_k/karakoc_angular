import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from '../../services/user.service';
import { NzMessageService } from "ng-zorro-antd/message";
import { NzFormControlComponent } from 'ng-zorro-antd/form';
import { UserQueries } from '../../services/user.queries';

class UserRegistrationFormModel {
  username = "";
  password = "";
  confirmPassword = "";
}

@Component({
  selector: 'app-user-registration',
  templateUrl: './user-registration.component.html',
  styleUrls: ['./user-registration.component.less']
})
export class UserRegistrationComponent implements OnInit {
  @ViewChild("f")
  form: NgForm;

  model = new UserRegistrationFormModel();

  constructor(
    private router: Router,
    private userService: UserService,
    private nzMessageService: NzMessageService,
    private userQueries: UserQueries
  ) { }

  ngOnInit(): void {
  }

  async submit() {

    if (this.form.form.invalid || this.model.password !== this.model.confirmPassword) {
      return;
    }
    else if(await this.userQueries.exists(this.model.username))
    {
      this.nzMessageService.error("L'identifiant est déja pris !");
      return;
    }

    try {

      let answerRegister : any = await this.userService.register(this.model.username, this.model.password);
      this.goToLogin();

    } catch (e) {
      this.nzMessageService.error("La connexion au serveur à échoué. Réessayez plus tard.");
    }
  }

  goToLogin() {
    this.router.navigateByUrl("/splash/login");
  }
}
